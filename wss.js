var {WebSocketServer} = require("ws");
var qs = require("qs");
var proxyaddr = require("proxy-addr");
var child_process = require("child_process");
var {app, server} = require("./web");
var {uncacheList} = require("./metalist");

var lastUpdate = Date.now();

var wss = new WebSocketServer({ server });

wss.on("connection", function(ws, req) {
	req.ip = proxyaddr(req, app.get("trust proxy"));
	req.query = qs.parse(req.url.substring(req.url.indexOf('?')+1));

	var log = (msg, e) => console[e?'error':'log'](`[${new Date().toLocaleString()}] ${req.ip} - ${msg}`);
	log("socket open: " + req.url);

	var url = req.query.url;
	if (!url) { ws.send(color("red", "missing url")); ws.close(); return }

	if (Date.now() - lastUpdate > 8.64e7) {
		ws.send("updating yt-dlp...");
		var cp = makeCp("pip", ["install", "--break-system-packages", "--upgrade", "yt-dlp"]);
		cp.on("close", next);
		lastUpdate = Date.now();
	} else next();

	function next() {
		var cp = makeCp("yt-dlp", ["--no-mtime", url], {cwd: process.env.DOWNLOADS, shell: false});
		cp.on("close", (code, signal) => {
			uncacheList();
			ws.close();
		});
		ws.on("close", () => {
			log("socket closed");
		});
	}

	function makeCp(command, args, options) {
		ws.send(color("green", `spawning ${command}...`));
		var cp = child_process.spawn(command, args, options);
	
		cp.on("error", error => {
			log(error.message, true);
			ws.send(color("red", error.message));
		});
		cp.stdout.on("data", data => {
			var msg = data.toString().trim();
			if (msg) {
				log(msg);
				ws.send(command + ": " + color("blue", msg));
			}
		});
		cp.stderr.on("data", data => {
			var msg = data.toString().trim();
			if (msg) {
				log(msg, true);
				ws.send(color("orange", command + ": " + color("blue", msg)));
			}
		});
		cp.on("close", (code, signal) => {
			log(`${command} exit ${code} (${signal})`);
			ws.send(color("green", `${command} exited with code ${code}`));
		});
		return cp;
	}
});

function color(color, text) {
	return `<span style="color: ${color}">${text}</span>`;
}