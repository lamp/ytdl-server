var getVideoLength = v => require('video-length')(v, {bin: "mediainfo"});
var getDiskInfo = require('diskusage').check;
var fs = require("fs");
var path = require("path");

var cached = {};

async function getList() {
	if (!cached.list || Date.now() - cached.time > 30000) {
		cached.list = await generateList();
		cached.diskinfo = await getDiskInfo(process.env.DOWNLOADS);
		cached.time = Date.now();
	}
	return cached;
}

async function generateList() {
	var files = fs.readdirSync(process.env.DOWNLOADS, {withFileTypes: true}).filter(f => f.isFile()).map(f => f.name);
	var list = await Promise.all(files.map(async name => {
		var filepath = path.join(process.env.DOWNLOADS, name);
		var metapath = path.join(process.env.DOWNLOADS, "metadata", name) + ".json";

		try {
			var metadata = JSON.parse(fs.readFileSync(metapath, "utf8"));
		} catch (error) {
			var metadata = {};
			try {
				metadata.duration = await getVideoLength(filepath);
			} catch(error) {console.error(error.message)};
			fs.writeFileSync(metapath, JSON.stringify(metadata));
		}

		var {mtime, size} = fs.statSync(filepath);
		return {name, mtime, size, duration: metadata.duration};
	}));

	list = list.sort((a,b) => a.mtime - b.mtime);
	return list;
}

function uncacheList() {
	cached = {};
}



module.exports = {getList, generateList, uncacheList};