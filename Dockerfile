FROM alpine:3.20
RUN adduser -S ytdl
RUN apk add --no-cache nodejs npm python3 py3-pip ffmpeg mediainfo make g++
ADD . /app
WORKDIR /app
RUN npm ci
ENV DOWNLOADS=/downloads
RUN mkdir -p $DOWNLOADS/metadata; chown -R ytdl $DOWNLOADS
USER ytdl
ENV PATH="/home/ytdl/.local/bin:$PATH"
VOLUME $DOWNLOADS
EXPOSE 8080
CMD ["sh", "-c", "pip install --break-system-packages --upgrade yt-dlp; exec node ."]